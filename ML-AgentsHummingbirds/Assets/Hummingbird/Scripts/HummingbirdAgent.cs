using System;
using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using UnityEngine;



public class HummingbirdAgent : Agent
{
    [Tooltip("Force to apply when moving")]
    public float moveForce = 2f;

    [Tooltip("Speed to pitch up or down")]
    public float pitchSpeed = 100f;

    [Tooltip("Speed to rotate around the up axis")]
    public float yawSpeed = 100f;

    [Tooltip("Transform at the tip of the beak")]
    public Transform beakTip;

    [Tooltip("The agent's camera")]
    public Camera agentCamera;

    [Tooltip("Whether this is training mode or gameplay mode")]
    public bool trainingMode;

    // El cuerpo r�gido de la agente
    new private Rigidbody rigidbody;

    // El �rea de flores en la que se encuentra la agente
    private FlowerArea flowerArea;

    //La flor m�s cercana a la agente
    private Flower nearestFlower;

    // permite cambios de tono m�s suaves
    private float smoothPitchChange = 0f;

    // Allows for smoother yaw changes
    private float smoothYawChange = 0f;

    // �ngulo m�ximo que el ave puede cabecear hacia arriba o hacia abajo
    private const float MaxPitchAngle = 80f;

    // Distancia m�xima desde la punta del pico para aceptar la colisi�n del n�ctar
    private const float BeakTipRadius = 0.008f;

    // Si la agente est� congelada (intencionalmente no vuela)
    private bool frozen = false;

    ///La cantidad de n�ctar que la agente ha obtenido en este episodio
    public float NectarObtained { get; private set; }


    /// Inicializar la agente

    public override void Initialize()
    {
        rigidbody = GetComponent<Rigidbody>();
        flowerArea = GetComponentInParent<FlowerArea>();

        // If not training mode, no max step, play forever
        if (!trainingMode) MaxStep = 0;
    }


    /// Restablecer la agente cuando comienza un episodio
    public override void OnEpisodeBegin()
    {
        if (trainingMode)
        {
            // Solo reinicia flores en entrenamiento cuando hay un agente por �rea
            flowerArea.ResetFlowers();
        }

        // Restablecer el n�ctar obtenido
        NectarObtained = 0f;

        // Poner a cero las velocidades para que el movimiento se detenga antes de que comience un nuevo episodio
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;

        // Predeterminado para desovar frente a una flor
        bool inFrontOfFlower = true;
        if (trainingMode)
        {
            // Aparece frente a la flor el 50% del tiempo durante el entrenamiento
            inFrontOfFlower = UnityEngine.Random.value > .5f;
        }

        // Mover la agente a una nueva posici�n aleatoria
        MoveToSafeRandomPosition(inFrontOfFlower);

        // Recalcular la flor m�s cercana ahora que el agente se ha movido
        UpdateNearestFlower();
    }

    /// <summary>
    /// Se llama cuando se recibe una acci�n de la entrada del jugador o de la red neuronal
    /// 
    /// vectorAction[i] represents:
    /// Index 0: move vector x (+1 = right, -1 = left)
    /// Index 1: move vector y (+1 = up, -1 = down)
    /// Index 2: move vector z (+1 = forward, -1 = backward)
    /// Index 3: pitch angle (+1 = pitch up, -1 = pitch down)
    /// Index 4: yaw angle (+1 = turn right, -1 = turn left)
    /// </summary>
    /// <param name="vectorAction">Las acciones a tomar</param>
    public override void OnActionReceived(float[] vectorAction)
    {
        // No tome medidas si est� congelado
        if (frozen) return;

        // Calcular vector de movimiento
        Vector3 move = new Vector3(vectorAction[0], vectorAction[1], vectorAction[2]);

        // Agregar fuerza en la direcci�n del vector de movimiento
        rigidbody.AddForce(move * moveForce);

        // Obtener la rotaci�n actual
        Vector3 rotationVector = transform.rotation.eulerAngles;

        // Calcular la rotaci�n de cabeceo y gui�ada
        float pitchChange = vectorAction[3];
        float yawChange = vectorAction[4];

        // Calcular cambios de rotaci�n suaves
        smoothPitchChange = Mathf.MoveTowards(smoothPitchChange, pitchChange, 2f * Time.fixedDeltaTime);
        smoothYawChange = Mathf.MoveTowards(smoothYawChange, yawChange, 2f * Time.fixedDeltaTime);

        // Calcule nuevos cabeceos y gui�adas basados en valores suavizados
        // Abrazadera de paso para evitar voltear boca abajo
        float pitch = rotationVector.x + smoothPitchChange * Time.fixedDeltaTime * pitchSpeed;
        if (pitch > 180f) pitch -= 360f;
        pitch = Mathf.Clamp(pitch, -MaxPitchAngle, MaxPitchAngle);

        float yaw = rotationVector.y + smoothYawChange * Time.fixedDeltaTime * yawSpeed;

        // Aplicar la nueva rotaci�n
        transform.rotation = Quaternion.Euler(pitch, yaw, 0f);
    }

    /// <summary>
    /// Recopile observaciones de vectores del entorno.
    /// </summary>
    /// <param name="sensor">The vector sensor</param>
    public override void CollectObservations(VectorSensor sensor)
    {
        // Si la flor m�s cercana es nula, observe una matriz vac�a y regrese temprano
        if (nearestFlower == null)
        {
            sensor.AddObservation(new float[10]);
            return;
        }

        // Observar la rotaci�n local del agente (4 observaciones)
        sensor.AddObservation(transform.localRotation.normalized);

        // Obtenga un vector desde la punta del pico hasta la flor m�s cercana
        Vector3 toFlower = nearestFlower.FlowerCenterPosition - beakTip.position;

        // Observa un vector normalizado que apunta a la flor m�s cercana (3 observaciones)
        sensor.AddObservation(toFlower.normalized);

        // Observe un producto escalar que indica si la punta del pico est� frente a la flor (1 observaci�n)
        // (+1 significa que la punta del pico est� directamente en frente de la flor, -1 significa directamente detr�s)
        sensor.AddObservation(Vector3.Dot(toFlower.normalized, -nearestFlower.FlowerUpVector.normalized));

        // Observa un producto escalar que indica si el pico apunta hacia la flor (1 observaci�n)
        // (+1 significa que el pico apunta directamente a la flor, -1 significa directamente lejos)
        sensor.AddObservation(Vector3.Dot(beakTip.forward.normalized, -nearestFlower.FlowerUpVector.normalized));

        // Observar la distancia relativa desde la punta del pico hasta la flor (1 observaci�n)
        sensor.AddObservation(toFlower.magnitude / FlowerArea.AreaDiameter);

        // 10 observaciones totales
    }

    /// <summary>
    /// Cuando el Tipo de comportamiento se establece en "Solo heur�stico" en los Par�metros de comportamiento del agente,
    /// esta funci�n ser� llamada. Sus valores de retorno se introducir�n en
    /// <see cref="OnActionReceived(float[])"/> en lugar de usar la red neuronal
    /// </summary>
    /// <param name="actionsOut">Y matriz de acci�n de salida</param>
    public override void Heuristic(float[] actionsOut)
    {
        // Crear marcadores de posici�n para todos los movimientos/giros
        Vector3 forward = Vector3.zero;
        Vector3 left = Vector3.zero;
        Vector3 up = Vector3.zero;
        float pitch = 0f;
        float yaw = 0f;

        // Convierta las entradas del teclado en movimiento y giro
        // Todos los valores deben estar entre -1 y +1

        // Hacia adelante hacia atr�s
        if (Input.GetKey(KeyCode.W)) forward = transform.forward;
        else if (Input.GetKey(KeyCode.S)) forward = -transform.forward;

        // Izquierda / derecha
        if (Input.GetKey(KeyCode.A)) left = -transform.right;
        else if (Input.GetKey(KeyCode.D)) left = transform.right;

        //Arriba abajo
        if (Input.GetKey(KeyCode.E)) up = transform.up;
        else if (Input.GetKey(KeyCode.C)) up = -transform.up;

        // Inclinaci�n arriba/abajo
        if (Input.GetKey(KeyCode.UpArrow)) pitch = 1f;
        else if (Input.GetKey(KeyCode.DownArrow)) pitch = -1f;

        // Voltee izquierda derecha
        if (Input.GetKey(KeyCode.LeftArrow)) yaw = -1f;
        else if (Input.GetKey(KeyCode.RightArrow)) yaw = 1f;

        // Combinar los vectores de movimiento y normalizar
        Vector3 combined = (forward + left + up).normalized;

        // Agregue los 3 valores de movimiento, cabeceo y gui�ada a la matriz actionsOut
        actionsOut[0] = combined.x;
        actionsOut[1] = combined.y;
        actionsOut[2] = combined.z;
        actionsOut[3] = pitch;
        actionsOut[4] = yaw;
    }


    /// Impedir que el agente se mueva y realice acciones

    public void FreezeAgent()
    {
        Debug.Assert(trainingMode == false, "Freeze/Unfreeze not supported in training");
        frozen = true;
        rigidbody.Sleep();
    }


    /// Reanudar el movimiento y las acciones del agente

    public void UnfreezeAgent()
    {
        Debug.Assert(trainingMode == false, "Freeze/Unfreeze not supported in training");
        frozen = false;
        rigidbody.WakeUp();
    }


    /// Mueve al agente a una posici�n aleatoria segura (es decir, no choca con nada)
    /// Si est� frente a la flor, tambi�n apunte el pico hacia la flor.

    /// <param name="inFrontOfFlower">Ya sea para elegir un lugar frente a una flor</param>
    private void MoveToSafeRandomPosition(bool inFrontOfFlower)
    {
        bool safePositionFound = false;
        int attemptsRemaining = 100; //Evitar un bucle infinito
        Vector3 potentialPosition = Vector3.zero;
        Quaternion potentialRotation = new Quaternion();

        // Bucle hasta que se encuentre una posici�n segura o nos quedemos sin intentos
        while (!safePositionFound && attemptsRemaining > 0)
        {
            attemptsRemaining--;
            if (inFrontOfFlower)
            {
                // Elige una flor al azar
                Flower randomFlower = flowerArea.Flowers[UnityEngine.Random.Range(0, flowerArea.Flowers.Count)];

                // Posici�n de 10 a 20 cm delante de la flor.
                float distanceFromFlower = UnityEngine.Random.Range(.1f, .2f);
                potentialPosition = randomFlower.transform.position + randomFlower.FlowerUpVector * distanceFromFlower;

                // Apunte el pico a la flor (la cabeza del p�jaro es el centro de la transformaci�n)
                Vector3 toFlower = randomFlower.FlowerCenterPosition - potentialPosition;
                potentialRotation = Quaternion.LookRotation(toFlower, Vector3.up);
            }
            else
            {
                //Elija una altura aleatoria desde el suelo
                float height = UnityEngine.Random.Range(1.2f, 2.5f);

                //Elija un radio aleatorio desde el centro del �rea
                float radius = UnityEngine.Random.Range(2f, 7f);

                // Elija una direcci�n aleatoria girada alrededor del eje y
                Quaternion direction = Quaternion.Euler(0f, UnityEngine.Random.Range(-180f, 180f), 0f);

                // Combine la altura, el radio y la direcci�n para elegir una posici�n potencial
                potentialPosition = flowerArea.transform.position + Vector3.up * height + direction * Vector3.forward * radius;

                //Elija y configure el cabeceo inicial aleatorio y la gui�ada
                float pitch = UnityEngine.Random.Range(-60f, 60f);
                float yaw = UnityEngine.Random.Range(-180f, 180f);
                potentialRotation = Quaternion.Euler(pitch, yaw, 0f);
            }

            // Verifique si el agente chocar� con algo
            Collider[] colliders = Physics.OverlapSphere(potentialPosition, 0.05f);

            // Se ha encontrado una posici�n segura si no hay colisionadores superpuestos
            safePositionFound = colliders.Length == 0;
        }

        Debug.Assert(safePositionFound, "Could not find a safe position to spawn");

        // Establecer la posici�n y la rotaci�n
        transform.position = potentialPosition;
        transform.rotation = potentialRotation;
    }


    ///Actualizar la flor m�s cercana al agente

    private void UpdateNearestFlower()
    {
        foreach (Flower flower in flowerArea.Flowers)
        {
            if (nearestFlower == null && flower.HasNectar)
            {
                // No hay una flor m�s cercana actual y esta flor tiene n�ctar, as� que configure esta flor
                nearestFlower = flower;
            }
            else if (flower.HasNectar)
            {
                // Calcular la distancia a esta flor y la distancia a la flor actual m�s cercana
                float distanceToFlower = Vector3.Distance(flower.transform.position, beakTip.position);
                float distanceToCurrentNearestFlower = Vector3.Distance(nearestFlower.transform.position, beakTip.position);

                //Si la flor m�s cercana actual est� vac�a O esta flor est� m�s cerca, actualice la flor m�s cercana
                if (!nearestFlower.HasNectar || distanceToFlower < distanceToCurrentNearestFlower)
                {
                    nearestFlower = flower;
                }
            }
        }
    }


    /// Se llama cuando el colisionador del agente entra en un colisionador desencadenante

    /// <param name="other">El gatillo colisionador</param>
    private void OnTriggerEnter(Collider other)
    {
        TriggerEnterOrStay(other);
    }


    /// Llamado cuando el colisionador del agente permanece en un colisionador desencadenante

    /// <param name="other">El gatillo colisionador</param>
    private void OnTriggerStay(Collider other)
    {
        TriggerEnterOrStay(other);
    }


    /// Maneja cuando el colisionador del agente entra o permanece en un colisionador de activaci�n

    /// <param name="collider">El gatillo colisionador</param>
    private void TriggerEnterOrStay(Collider collider)
    {
        // Check if agent is colliding with nectar
        if (collider.CompareTag("nectar"))
        {
            Vector3 closestPointToBeakTip = collider.ClosestPoint(beakTip.position);

            // Compruebe si el punto de colisi�n m�s cercano est� cerca de la punta del pico
            // Nota: una colisi�n con cualquier cosa que no sea la punta del pico no debe contar
            if (Vector3.Distance(beakTip.position, closestPointToBeakTip) < BeakTipRadius)
            {
                // Busca la flor de este colisionador de n�ctar
                Flower flower = flowerArea.GetFlowerFromNectar(collider);

                // Intento de tomar .01 n�ctar
                // Nota: esto es por intervalo de tiempo fijo, lo que significa que sucede cada 0,02 segundos o 50 veces por segundo
                float nectarReceived = flower.Feed(.01f);

                // Realizar un seguimiento del n�ctar obtenid
                NectarObtained += nectarReceived;

                if (trainingMode)
                {
                    // Calcular recompensa por obtener n�ctar
                    float bonus = .02f * Mathf.Clamp01(Vector3.Dot(transform.forward.normalized, -nearestFlower.FlowerUpVector.normalized));
                    AddReward(.01f + bonus);
                }

                // Si la flor est� vac�a, actualice la flor m�s cercana
                if (!flower.HasNectar)
                {
                    UpdateNearestFlower();
                }
            }
        }
    }


    /// Llamado cuando el agente choca con algo s�lido

    /// <param name="collision">La informaci�n de la colisi�n</param>
    private void OnCollisionEnter(Collision collision)
    {
        if (trainingMode && collision.collider.CompareTag("boundary"))
        {
            //Choc� con el l�mite del �rea, da una recompensa negativa
            AddReward(-.5f);
        }
    }

 
    /// Llamado a cada cuadro

    private void Update()
    {
        // Draw a line from the beak tip to the nearest flower
        if (nearestFlower != null)
            Debug.DrawLine(beakTip.position, nearestFlower.FlowerCenterPosition, Color.green);
    }


    /// Llamado cada 0,02 segundos

    private void FixedUpdate()
    {
        // Evita el escenario en el que el oponente roba el n�ctar de la flor m�s cercana y no lo actualiza
        if (nearestFlower != null && !nearestFlower.HasNectar)
            UpdateNearestFlower();
    }

}
