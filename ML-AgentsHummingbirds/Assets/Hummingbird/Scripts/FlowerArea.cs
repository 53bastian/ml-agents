using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    //Administra una colecci�n de plantas de flores y flores adjuntas.
public class FlowerArea : MonoBehaviour
{
    // El di�metro del �rea donde el agente y las flores pueden ser
    // utilizado para observar la distancia relativa del agente a la flor
    public const float AreaDiameter = 20f;

    // La lista de todas las plantas con flores en esta �rea de flores (las plantas con flores tienen m�ltiples flores)
    private List<GameObject> flowerPlants;

    // Un diccionario de b�squeda para buscar una flor en un colisionador de n�ctar
    private Dictionary<Collider, Flower> nectarFlowerDictionary;

    /// La lista de todas las flores en el �rea de flores.
    public List<Flower> Flowers { get; private set; }

    /// Restablecer las flores y las plantas de flores.

    public void ResetFlowers()
    {
        // Gire cada planta de flores alrededor del eje Y y sutilmente alrededor de X y Z
        foreach (GameObject flowerPlant in flowerPlants)
        {
            float xRotation = UnityEngine.Random.Range(-5f, 5f);
            float yRotation = UnityEngine.Random.Range(-180f, 180f);
            float zRotation = UnityEngine.Random.Range(-5f, 5f);
            flowerPlant.transform.localRotation = Quaternion.Euler(xRotation, yRotation, zRotation);
        }

        // Restablecer cada flor
        foreach (Flower flower in Flowers)
        {
            flower.ResetFlower();
        }
    }


    /// obtiene <see cref="Flower"/> al que pertenece un colisionador de n�ctar

    /// <param name="collider">El colisionador de n�ctar</param>
    /// <returns>la flor a juego</returns>
    public Flower GetFlowerFromNectar(Collider collider)
    {
        return nectarFlowerDictionary[collider];
    }


    /// Llamado cuando el �rea se despierta

    private void Awake()
    {
        // Inicializar variables
        flowerPlants = new List<GameObject>();
        nectarFlowerDictionary = new Dictionary<Collider, Flower>();
        Flowers = new List<Flower>();

        // Encuentra todas las flores que son hijos de este GameObject/Transform
        FindChildFlowers(transform);
    }


    /// Encuentra recursivamente todas las flores y plantas de flores que son hijas de una transformaci�n principal

    /// <param name="parent">El padre de los ni�os para comprobar</param>
    private void FindChildFlowers(Transform parent)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            Transform child = parent.GetChild(i);

            if (child.CompareTag("flower_plant"))
            {
                // Encontr� una planta de flores, agr�guela a la lista de plantas de flores
                flowerPlants.Add(child.gameObject);

                // Busque flores dentro de la planta de flores
                FindChildFlowers(child);
            }
            else
            {
                // No es una planta de flor, busque un componente de flor
                Flower flower = child.GetComponent<Flower>();
                if (flower != null)
                {
                    // Encontr� una flor, agr�guela a la lista de Flores
                    Flowers.Add(flower);

                    // Agregue el colisionador de n�ctar al diccionario de b�squeda
                    nectarFlowerDictionary.Add(flower.nectarCollider, flower);

                    //Nota: no hay flores que sean hijas de otras flores
                }
                else
                {
                    // No se encontr� el componente de la flor, as� que verifique a los ni�os
                    FindChildFlowers(child);
                }
            }
        }
    }
}
