using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// Maneja una sola flor con n�ctar
public class Flower : MonoBehaviour
{
    [Tooltip("The color when the flower is full")]
    public Color fullFlowerColor = new Color(1f, 0f, .3f);

    [Tooltip("The color when the flower is empty")]
    public Color emptyFlowerColor = new Color(.5f, 0f, 1f);

    /// El gatillo colisionador que representa el n�ctar 
    [HideInInspector]
    public Collider nectarCollider;

    //El colisionador s�lido que representa los p�talos de las flores
    private Collider flowerCollider;

    // el material de la flor
    private Material flowerMaterial;

    //Un vector que apunta directamente desde la flor
    public Vector3 FlowerUpVector
    {
        get
        {
            return nectarCollider.transform.up;
        }
    }


    // La posici�n central del colisionador de n�ctar
    public Vector3 FlowerCenterPosition
    {
        get
        {
            return nectarCollider.transform.position;
        }
    }


    // La cantidad de n�ctar que queda en la flor.
    public float NectarAmount { get; private set; }


    /// Si a la flor le queda n�ctar
    public bool HasNectar
    {
        get
        {
            return NectarAmount > 0f;
        }
    }


    /// Intentos de quitar el n�ctar de la flor.
    /// <param name="amount">La cantidad de n�ctar a eliminar</param>
    /// <returns>La cantidad real eliminada con �xito</returns>
    public float Feed(float amount)
    {
        // Haga un seguimiento de la cantidad de n�ctar que se tom� con �xito (no puede tomar m�s de lo que est� disponible)
        float nectarTaken = Mathf.Clamp(amount, 0f, NectarAmount);

        // Restar el n�ctar
        NectarAmount -= amount;

        if (NectarAmount <= 0)
        {
            //No queda n�ctar
            NectarAmount = 0;

            //Deshabilite los colisionadores de flores y n�ctar.
            flowerCollider.gameObject.SetActive(false);
            nectarCollider.gameObject.SetActive(false);

            //Cambia el color de la flor para indicar que est� vac�a.
            flowerMaterial.SetColor("_BaseColor", emptyFlowerColor);
        }

        //Devolver la cantidad de n�ctar que se tom�
        return nectarTaken;
    }


    /// Restablece la flor
    public void ResetFlower()
    {
        //Rellenar el n�ctar
        NectarAmount = 1f;

        // Habilitar los colisionadores de flores y n�ctar
        flowerCollider.gameObject.SetActive(true);
        nectarCollider.gameObject.SetActive(true);

        //Cambia el color de la flor para indicar que est� lleno
        flowerMaterial.SetColor("_BaseColor", fullFlowerColor);
    }


    /// Llamado cuando la flor se despierta
    private void Awake()
    {
        // Encuentra el renderizador de malla de la flor y obt�n el material principal.
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        flowerMaterial = meshRenderer.material;

        // Encuentra colisionadores de flores y n�ctar
        flowerCollider = transform.Find("FlowerCollider").GetComponent<Collider>();
        nectarCollider = transform.Find("FlowerNectarCollider").GetComponent<Collider>();
    }

}
